/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "mod_common.h"
#include "version_firmware.h"

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#define ME "deviceinfo"

static void kill_fit_tools(void) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxp_subproc_t* proc = NULL;

    when_failed_trace(access(SCRIPT_KILL_FIT_TOOLS, F_OK), exit, ERROR, "Script '%s' doesn't exist.", SCRIPT_KILL_FIT_TOOLS);

    rv = amxp_subproc_new(&proc);
    when_failed_trace(rv, exit, ERROR, "Failed to initialize the subprocess.");

    rv = amxp_subproc_start(proc, (char*) "sh", SCRIPT_KILL_FIT_TOOLS, NULL);
    when_failed_trace(rv, exit, ERROR, "Failed to run the shell script.");

    amxp_subproc_wait(proc, 1000);
    amxp_subproc_kill(proc, SIGKILL);

exit:
    amxp_subproc_delete(&proc);
    SAH_TRACEZ_OUT(ME);
}

static cstring_t get_rootfs_version(rootfs_type_t rootfs_type) {
    SAH_TRACEZ_IN(ME);
    int script_parameter = 0;
    amxc_string_t* string_stdout = NULL;
    amxc_string_t* string_script_param = NULL;
    amxc_string_t* string_regex = NULL;
    cstring_t script_output = NULL;

    if(rootfs_type == rootfs_inactive) {
        script_parameter = 1;
    }

    amxc_string_new(&string_stdout, 0);
    amxc_string_new(&string_script_param, 0);
    amxc_string_new(&string_regex, 0);

    amxc_string_setf(string_script_param, "%d", script_parameter);

    script_output = execute_script_get_output(SCRIPT_VERSION_ROOTFS, string_script_param);
    when_true(STRING_EMPTY(script_output), exit);

exit:
    amxc_string_delete(&string_stdout);
    amxc_string_delete(&string_script_param);
    amxc_string_delete(&string_regex);
    kill_fit_tools();
    SAH_TRACEZ_OUT(ME);
    return script_output;
}

static cstring_t get_rootfs_partlabel(rootfs_type_t rootfs_type) {
    SAH_TRACEZ_IN(ME);
    int script_parameter = 0;
    amxc_string_t* string_script_param = NULL;
    cstring_t script_output = NULL;

    if(rootfs_type == rootfs_inactive) {
        script_parameter = 1;
    }

    amxc_string_new(&string_script_param, 0);

    amxc_string_setf(string_script_param, "%d", script_parameter);

    script_output = execute_script_get_output(SCRIPT_PARTLABEL, string_script_param);
    when_str_empty(script_output, exit);

exit:
    amxc_string_delete(&string_script_param);
    kill_fit_tools();
    SAH_TRACEZ_OUT(ME);
    return script_output;
}

int get_firmware_info(UNUSED const char* function_name,
                      amxc_var_t* args,
                      amxc_var_t* ret) {
    int rv = -1;
    cstring_t rootfs_version = NULL;
    cstring_t partlabel = NULL;
    amxc_string_t string_version;
    int index = GET_INT32(args, "index");
    rootfs_type_t rootfs_type = rootfs_active;

    amxc_string_init(&string_version, 0);

    when_true_trace((index <= 0) && (index > 2), exit, ERROR, "Index is not 1 or 2");
    if(index == 2) {
        rootfs_type = rootfs_inactive;
    }

    partlabel = get_rootfs_partlabel(rootfs_type);
    if(!STRING_EMPTY(partlabel)) {
        amxc_var_add_key(cstring_t, ret, "name", partlabel);
    }

    rootfs_version = get_rootfs_version(rootfs_type);
    if(!STRING_EMPTY(rootfs_version)) {
        amxc_var_add_key(cstring_t, ret, "version", rootfs_version);
    }

    rv = 0;
exit:
    free(rootfs_version);
    free(partlabel);
    amxc_string_clean(&string_version);
    return rv;
}
