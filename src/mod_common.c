/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <regex.h>
#include <unistd.h>
#include <sys/stat.h>

#include "mod_common.h"

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <filetransfer/filetransfer.h>

#define ME "deviceinfo"
#define STDOUT_BUFFER_SIZE 9128

typedef enum _tmpfs_script_type {
    MOUNT,
    UNMOUNT
} tmpfs_script_type_t;

static amxc_string_t* fwi_path = NULL;
static amxo_parser_t* parser = NULL;
static amxd_dm_t* dm = NULL;
static bool ftx_initialized = false;
static bool busy = false;

void set_busy_status(bool parameter_busy) {
    busy = parameter_busy;
}

bool get_busy_status(void) {
    return busy;
}

int is_path_directory(const char* path) {
    struct stat statbuf;
    if(STRING_EMPTY(path)) {
        return 0;
    }
    if(stat(path, &statbuf) != 0) {
        return 0;
    }
    return S_ISDIR(statbuf.st_mode);
}

void delete_file(const char* file) {
    when_str_empty(file, exit);
    if(access(file, F_OK) == 0) {
        remove(file);
    }
exit:
    return;
}

void set_fwi_path(const char* path) {
    if(fwi_path == NULL) {
        amxc_string_new(&fwi_path, 0);
    }
    amxc_string_setf(fwi_path, "%s", path);
}

void clean_fwi_path(void) {
    amxc_string_delete(&fwi_path);
    fwi_path = NULL;
}

void set_fwi_status(const char* status, const char* faultString) {
    int retval = -1;
    amxc_var_t ret;
    amxc_var_t input_args;

    amxc_var_init(&ret);
    amxc_var_init(&input_args);
    amxc_var_set_type(&input_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &input_args, "Status", status);
    amxc_var_add_key(cstring_t, &input_args, "FwiPath", amxc_string_get(fwi_path, 0));
    amxc_var_add_key(cstring_t, &input_args, "FaultString", faultString);

    retval = amxm_execute_function(NULL, MOD_DM_MNGR, "update-status", &input_args, &ret);
    if(retval != 0) {
        SAH_TRACEZ_WARNING(ME, "Mod %s, func 'update-status' returned %d", MOD_DM_MNGR, retval);
    }
    amxc_var_clean(&ret);
    amxc_var_clean(&input_args);
}

int get_fwi_index(void) {
    int index = 0;
    amxd_object_t* fwi_obj = NULL;

    when_null_trace(fwi_path, exit, INFO, "fwi_path is NULL.");
    when_str_empty_trace(amxc_string_get(fwi_path, 0), exit, INFO, "fwi_path buffer is empty or NULL.");
    fwi_obj = amxd_dm_findf(dm, "%s", amxc_string_get(fwi_path, 0));
    when_null_trace(fwi_obj, exit, INFO, "FirmwareImage object NULL.");
    index = amxd_object_get_index(fwi_obj);

exit:
    return index;
}

static void filetransfer_event_cb(int fd, UNUSED void* priv) {
    ftx_fd_event_handler(fd);
}

static int connection_cb(int fd, bool add) {
    int retval = 0;
    if(add) {
        retval = amxo_connection_add(parser, fd, filetransfer_event_cb, NULL, AMXO_LISTEN, NULL);
    } else {
        retval = amxo_connection_remove(parser, fd);
    }
    SAH_TRACEZ_INFO(ME, "%s connection fd %d %s", add ? "add" : "remove", fd, (retval == 0) ? "success" : "failure");
    return retval;
}

void init_filetransfer_ctx(int (* cb_function)(int, bool)) {
    if(!ftx_initialized) {
        ftx_init(cb_function);
        ftx_initialized = true;
    }
}

void clean_filetransfer_ctx(void) {
    ftx_clean();
}

int set_parser(UNUSED const char* function_name,
               amxc_var_t* args,
               UNUSED amxc_var_t* ret) {
    int status = -1;
    parser = (amxo_parser_t*) args;
    if(parser != NULL) {
        status = 0;
    }
    init_filetransfer_ctx(connection_cb);
    return status;
}

amxo_parser_t* get_deviceinfo_parser(void) {
    return parser;
}

int set_dm(UNUSED const char* function_name,
           amxc_var_t* args,
           UNUSED amxc_var_t* ret) {
    int status = -1;
    dm = (amxd_dm_t*) args;
    if(dm != NULL) {
        status = 0;
    }
    init_filetransfer_ctx(connection_cb);
    return status;
}

amxd_dm_t* get_dm(void) {
    return dm;
}

const char* get_config(const char* config_param) {
    const char* value = GET_CHAR(amxo_parser_get_config(parser, config_param), NULL);
    return value != NULL ? value : "";
}

int32_t get_config_int32(const char* config_param) {
    int32_t value = GET_INT32(amxo_parser_get_config(parser, config_param), NULL);
    return value;
}

uint32_t get_uint32_param(const char* parameter, bool with_prefix) {
    amxd_object_t* deviceinfo = amxd_dm_findf(dm, "DeviceInfo.");
    amxc_string_t param;
    uint32_t value = 0;

    amxc_string_init(&param, 0);

    when_null(deviceinfo, exit);
    amxc_string_setf(&param, "%s%s", with_prefix ? get_config("vendor_prefix") : "", parameter);
    value = GET_UINT32(amxd_object_get_param_value(deviceinfo, amxc_string_get(&param, 0)), NULL);

exit:
    amxc_string_clean(&param);
    return value;
}

char* get_swu_path(void) {
    SAH_TRACEZ_IN(ME);
    amxc_string_t path_swu;
    char* fullpath_tmpfs = realpath(get_config("download_tmpfs_partition"), NULL);
    const char* path_tmpfs = (STRING_EMPTY(fullpath_tmpfs) ? get_config("download_tmpfs_partition") : fullpath_tmpfs);
    char* ret_str = NULL;

    amxc_string_init(&path_swu, 0);

    when_str_empty_trace(path_tmpfs, exit, ERROR, "tmpfs partition is not defined.")
    amxc_string_setf(&path_swu, "%s/"FLASH_SWU_FILE, path_tmpfs);

    ret_str = amxc_string_take_buffer(&path_swu);
exit:
    amxc_string_clean(&path_swu);
    free(fullpath_tmpfs);
    SAH_TRACEZ_OUT(ME);
    return ret_str;
}

/**
   @brief
   Read the information from the file and save to a string.

   @param[in] fname file name.
   @param[out] string string containing the information of the file appended.
 */
void read_from_file(const char* fname, amxc_string_t* string) {
    SAH_TRACEZ_IN(ME);
    FILE* p_file = NULL;
    cstring_t buffer = NULL;
    long length = 0;
    size_t bytes_read = 0;

    when_str_empty_trace(fname, exit, ERROR, "Filename is empty.");
    when_null_trace(string, exit, ERROR, "String is NULL.");
    amxc_string_reset(string);

    p_file = fopen(fname, "rb");
    when_null(p_file, exit);

    fseek(p_file, 0, SEEK_END);
    length = ftell(p_file);
    fseek(p_file, 0, SEEK_SET);
    buffer = calloc(1, length + 1);
    when_null_trace(buffer, exit, ERROR, "Could not allocate memory to the buffer");
    bytes_read = fread(buffer, 1, length, p_file);

    if(bytes_read <= 0) {
        free(buffer);
        goto exit;
    }
    amxc_string_push_buffer(string, buffer, length + 1);

exit:
    if(p_file != NULL) {
        fclose(p_file);
    }
    SAH_TRACEZ_OUT(ME);
}

/**
   @brief
   Read the information from the file descriptor and save to a string.

   @param[in] fd file descriptor.
   @param[out] string string containing the information of the fd appended.
 */
void read_from_fd(const int fd, amxc_string_t* string) {
    SAH_TRACEZ_IN(ME);
    char buffer[STDOUT_BUFFER_SIZE];
    ssize_t bytes = 0;

    when_null_trace(string, exit, ERROR, "Output string is not initialised.");
    when_false_trace(fd > 0, exit, ERROR, "File descriptor is not bigger than 0.");

    bytes = read(fd, buffer, STDOUT_BUFFER_SIZE);
    when_true(((bytes <= 0) || (bytes >= STDOUT_BUFFER_SIZE)), exit);

    if(buffer[0] != '\0') {
        buffer[bytes] = 0;
        amxc_string_append(string, buffer, bytes);
    }

exit:
    SAH_TRACEZ_OUT(ME);
}

/**
   @brief
   Split a string in a list with a line for each element.

   @param[in] string Source string containing the information to be splitted.
   @param[out] lines List containing a line for each element.
 */
void split_string_lines(amxc_string_t* string, amxc_llist_t* lines) {
    SAH_TRACEZ_IN(ME);
    cstring_t cur_line;
    cstring_t next_line;

    when_null_trace(lines, exit, ERROR, "Output list is not initialised.");
    when_null_trace(string, exit, ERROR, "String to be splitted is NULL.");
    when_false_trace(string->length > 0, exit, ERROR, "String to be splitted is empty.");

    cur_line = string->buffer;

    while(cur_line) {
        next_line = strchr(cur_line, '\n');
        if(next_line) {
            *next_line = '\0';
        }

        amxc_llist_add_string(lines, cur_line);

        if(next_line) {
            *next_line = '\n';
        }
        cur_line = next_line ? (next_line + 1) : NULL;
    }

exit:
    SAH_TRACEZ_OUT(ME);
}

/**
   @brief
   Reboot the board.
 */
void reboot_board(void) {
    int rv = -1;
    amxp_subproc_t* proc = NULL;
    amxb_bus_ctx_t* bus_ctx = NULL;
    amxc_var_t args;
    amxc_var_t ret;

    SAH_TRACEZ_WARNING(ME, "Rebooting the board.");

    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_init(&ret);

    bus_ctx = amxb_be_who_has("Device.");
    when_null_trace(bus_ctx, system_reboot, ERROR, "Failed to get the context bus.");

    amxc_var_add_key(cstring_t, &args, "Cause", "LocalReboot");
    amxc_var_add_key(cstring_t, &args, "Reason", "Firmware Upgrade");
    rv = amxb_call(bus_ctx, "Device.", "Reboot", &args, &ret, 5);
    when_failed_trace(rv, exit, ERROR, "Failed to call Device.Reboot() for object: %d", rv);

    when_true(rv == 0, exit);

system_reboot:
    SAH_TRACEZ_WARNING(ME, "Something went wrong with TR-181 reboot, performing a system reboot.");

    rv = amxp_subproc_new(&proc);
    when_failed_trace(rv, exit, ERROR, "Failed to initialize the subprocess to reboot.");

    rv = amxp_subproc_start_wait(proc, 1000, (char*) "reboot", NULL);
    when_failed_trace(rv, exit, ERROR, "Failed to run the reboot command.");

    if(amxp_subproc_get_exitstatus(proc) != 0) {
        SAH_TRACEZ_WARNING(ME, "Failed to reboot the board.");
    }

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxp_subproc_delete(&proc);
    return;
}

/**
   @brief
   Extract a substring with a regular expression match.

   @param[in] str Source string containing the information to be searched.
   @param[in] regex Regular expression.
   @param[out] extracted_str Substring matched.

   @return
   0 if there is a match with the regex. Not 0 if there isn't a match.
 */
int extract_regex_from_string(amxc_string_t* str,
                              const cstring_t regex,
                              amxc_string_t* extracted_str) {
    SAH_TRACEZ_IN(ME);
    regex_t finder;
    size_t match_size = 0;
    cstring_t substr = NULL;
    int ret = -1;
    int cflags = REG_EXTENDED;
    regmatch_t pmatch[2] = {{-1, -1}, {-1, -1}};

    when_null_trace(extracted_str, exit, ERROR, "Extracted string is not initialised.");
    when_true_trace(amxc_string_is_empty(str), exit, ERROR, "String to be searched is NULL.");
    when_false_trace(str->length > 0, exit, ERROR, "String to be searched is empty.");
    when_null_trace(regex, exit, ERROR, "Regular expression is NULL");

    ret = regcomp(&finder, regex, cflags);
    when_failed(ret, clean);

    ret = regexec(&finder, str->buffer, 1, pmatch, 0);
    when_failed(ret, clean);

    match_size = pmatch[0].rm_eo - pmatch[0].rm_so;
    substr = amxc_string_dup(str, pmatch[0].rm_so, match_size);
    amxc_string_push_buffer(extracted_str, substr, match_size + 1);

clean:
    regfree(&finder);
exit:
    SAH_TRACEZ_OUT(ME);
    return ret;
}

cstring_t execute_script_get_output(const cstring_t script, amxc_string_t* string_script_param) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxp_subproc_t* proc = NULL;
    amxc_string_t* string_stdout = NULL;
    int32_t fd = -1;
    cstring_t ret_string = NULL;
    int counter = 0;

    amxc_string_new(&string_stdout, 0);

    when_failed_trace(access(script, F_OK), exit, ERROR, "Script '%s' doesn't exist.", script);

    rv = amxp_subproc_new(&proc);
    when_failed_trace(rv, exit, ERROR, "Failed to initialize the subprocess.");

    fd = amxp_subproc_open_fd(proc, STDOUT_FILENO);
    when_false_trace(fd > 0, exit, ERROR, "Failed to open file descriptor.");

    if(STRING_EMPTY(amxc_string_get(string_script_param, 0))) {
        rv = amxp_subproc_start(proc, (char*) "sh", script, NULL);
    } else {
        rv = amxp_subproc_start(proc, (char*) "sh", script, amxc_string_get(string_script_param, 0), NULL);
    }
    when_failed_trace(rv, exit, ERROR, "Failed to run the shell script.");

    do {
        read_from_fd(fd, string_stdout);
        if(amxc_string_search(string_stdout, STRING_END_OF_SCRIPT, 0) != -1) {
            amxp_subproc_kill(proc, SIGKILL);
            break;
        }
        if(counter > 3000) {
            break;
        }
        counter++;
    } while (amxp_subproc_wait(proc, 1) == 1);
    amxp_subproc_kill(proc, SIGKILL);

    amxc_string_replace(string_stdout, STRING_END_OF_SCRIPT, "", UINT32_MAX);
    amxc_string_replace(string_stdout, "\n", "", UINT32_MAX);
    amxc_string_replace(string_stdout, "\r", "", UINT32_MAX);
    amxc_string_replace(string_stdout, " ", "", UINT32_MAX);

    when_str_empty_trace(amxc_string_get(string_stdout, 0), exit, ERROR, "Output string is empty.");
    ret_string = amxc_string_dup(string_stdout, 0, amxc_string_text_length(string_stdout));

exit:
    amxp_subproc_delete(&proc);
    amxc_string_delete(&string_stdout);
    SAH_TRACEZ_OUT(ME);
    return ret_string;
}

int execute_tmpfs_script_get_output(const cstring_t script, int timeout_ms, amxc_string_t* output,
                                    const cstring_t p1, const cstring_t p2, const cstring_t p3, const cstring_t p4, const cstring_t p5) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxp_subproc_t* proc = NULL;
    int32_t fd_stdout = -1;
    int32_t fd_stderr = -1;
    amxc_llist_t param_list;
    int ret_int = -1;
    int counter = 0;
    bool file_locked = false;

    amxc_llist_init(&param_list);

    when_null_trace(output, exit, ERROR, "Output string is NULL");
    when_str_empty_trace(script, exit, ERROR, "Script path is an empty string or NULL.");
    when_failed_trace(access(script, F_OK), exit, ERROR, "Script '%s' doesn't exist.", script);

    rv = amxp_subproc_new(&proc);
    when_failed_trace(rv, exit, ERROR, "Failed to initialize the subprocess.");

    fd_stdout = amxp_subproc_open_fd(proc, STDOUT_FILENO);
    when_false_trace(fd_stdout > 0, exit, ERROR, "Failed to open file descriptor.");

    fd_stderr = amxp_subproc_open_fd(proc, STDERR_FILENO);
    when_false_trace(fd_stderr > 0, exit, ERROR, "Failed to open file descriptor.");

    if(access(TMPFS_SCRIPT_LOCK_FILE, F_OK) == 0) {
        file_locked = true;
    }

    rv = amxp_subproc_start(proc, (char*) "sh", script, p1, p2, p3, p4, p5, NULL);
    when_failed_trace(rv, exit, ERROR, "Failed to run the shell script.");

    do {
        read_from_fd(fd_stdout, output);
        read_from_fd(fd_stderr, output);
        if(counter > timeout_ms) {
            SAH_TRACEZ_ERROR(ME, "Timeout reached to execute the script %s", script);
            break;
        }
        counter++;
    } while (amxp_subproc_wait(proc, 1) == 1);
    ret_int = amxp_subproc_get_exitstatus(proc);
    amxp_subproc_kill(proc, SIGKILL);

    if(!file_locked && (access(TMPFS_SCRIPT_LOCK_FILE, F_OK) == 0)) {
        remove(TMPFS_SCRIPT_LOCK_FILE);
    }

exit:
    amxp_subproc_delete(&proc);
    amxc_llist_clean(&param_list, amxc_string_list_it_free);
    SAH_TRACEZ_OUT(ME);
    return ret_int;
}

static int _tmpfs_partition(const char* user, unsigned int file_size_mb, tmpfs_script_type_t script_type) {
    SAH_TRACEZ_IN(ME);
    amxc_string_t* string_stdout = NULL;
    amxc_string_t file_size_str;
    amxc_string_t param_size_str;
    amxc_string_t param_duration_str;
    int ret_status = -1;
    const char* param_path = get_config("download_tmpfs_partition");
    uint32_t param_size = get_config_int32("download_tmpfs_size_mb");
    uint32_t param_duration = get_uint32_param(TMPFS_DOWNLOAD_DURATION_PARAM, true);
    const char* script = script_type == MOUNT ? TMPFS_SCRIPT_MOUNT_PATH : TMPFS_SCRIPT_UNMOUNT_PATH;

    amxc_string_new(&string_stdout, 0);
    amxc_string_init(&file_size_str, 0);
    amxc_string_init(&param_size_str, 0);
    amxc_string_init(&param_duration_str, 0);

    when_str_empty_trace(param_path, exit, ERROR, "download_tmpfs_partition is empty string.");
    when_false_trace(param_size > 0, exit, ERROR, "download_tmpfs_size_mb is 0.");
    when_false_trace(param_duration > 0, exit, ERROR, TMPFS_DOWNLOAD_DURATION_PARAM " is 0.");

    amxc_string_setf(&file_size_str, "%d", file_size_mb);
    amxc_string_setf(&param_size_str, "%d", param_size);
    amxc_string_setf(&param_duration_str, "%d", param_duration);
    if(script_type == MOUNT) {
        ret_status = execute_tmpfs_script_get_output(script, 5000, string_stdout, user, amxc_string_get(&file_size_str, 0), param_path, amxc_string_get(&param_size_str, 0), amxc_string_get(&param_duration_str, 0));
    } else {
        ret_status = execute_tmpfs_script_get_output(script, 5000, string_stdout, user, param_path, amxc_string_get(&param_size_str, 0), amxc_string_get(&param_duration_str, 0), NULL);
    }

    if(ret_status != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to execute script '%s'. Output from script: %s", script, amxc_string_get(string_stdout, 0));
    }

exit:
    amxc_string_delete(&string_stdout);
    amxc_string_clean(&file_size_str);
    amxc_string_clean(&param_size_str);
    amxc_string_clean(&param_duration_str);
    SAH_TRACEZ_OUT(ME);
    return ret_status;
}

int mount_tmpfs_partition(const char* user, unsigned int file_size_mb) {
    SAH_TRACEZ_WARNING(ME, "Mounting tmpfs partition");

    return _tmpfs_partition(user, file_size_mb, MOUNT);
}

int unmount_tmpfs_partition(const char* user) {
    SAH_TRACEZ_WARNING(ME, "Unmounting tmpfs partition");

    return _tmpfs_partition(user, 0, UNMOUNT);
}
