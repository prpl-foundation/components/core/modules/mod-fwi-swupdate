/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <libgen.h>

#include "mod_common.h"
#include "download_firmware.h"
#include "flash_firmware.h"
#include <filetransfer/filetransfer.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#define ME "deviceinfo"
#define CB_AUTO_ACTIVATE "autoActivate"

static ftx_request_t* ftx_req = NULL;

static void delete_swu_file(const char* swu_file) {
    delete_file(swu_file);
}

static bool download_cb(UNUSED ftx_request_t* req, void* userdata) {
    int status = -1;
    amxc_var_t* callbackData = (amxc_var_t*) userdata;
    ftx_error_code_t error_code = ftx_request_get_error_code(ftx_req);
    bool auto_activate = false;

    if(error_code != ftx_error_code_no_error) {
        SAH_TRACEZ_WARNING(ME, "Download returned with errorcode %d", error_code);
        set_fwi_status(STATUS_DOWNFAILED, ftx_request_get_error_reason(ftx_req));
        set_busy_status(false);
        unmount_tmpfs_partition(TMPFS_SCRIPT_USER);
        goto exit;
    }
    SAH_TRACEZ_WARNING(ME, "Download image succeeded");
    auto_activate = GET_BOOL(callbackData, CB_AUTO_ACTIVATE);

    status = firmware_flash(auto_activate);
    if(status != 0) {
        set_fwi_status(STATUS_VALIDFAILED, "Failed to flash the firmware.");
        set_busy_status(false);
        unmount_tmpfs_partition(TMPFS_SCRIPT_USER);
    }

exit:
    amxc_var_delete(&callbackData);
    ftx_request_delete(&ftx_req);
    return true;
}

static char* get_folder_path(const char* file_path) {
    char* abs_path = getcwd(NULL, 0);
    char* dirname_path = NULL;
    char* folder_path = NULL;
    amxc_string_t abs_path_str;
    amxc_string_t folder_path_str;

    amxc_string_init(&abs_path_str, 0);
    amxc_string_init(&folder_path_str, 0);

    when_str_empty(abs_path, exit);
    amxc_string_setf(&abs_path_str, "%s", abs_path);

    if(file_path[0] != '/') {
        amxc_string_appendf(&abs_path_str, "/%s", file_path);
    } else {
        amxc_string_setf(&abs_path_str, "%s", file_path);
    }
    when_str_empty(amxc_string_get(&abs_path_str, 0), exit);

    free(abs_path);
    abs_path = amxc_string_dup(&abs_path_str, 0, UINT32_MAX);
    dirname_path = dirname(abs_path);
    amxc_string_setf(&folder_path_str, "%s", dirname_path);
    amxc_string_replace(&folder_path_str, "../", "/", UINT32_MAX);
    amxc_string_replace(&folder_path_str, "./", "/", UINT32_MAX);
    amxc_string_replace(&folder_path_str, "//", "/", UINT32_MAX);
    folder_path = amxc_string_dup(&folder_path_str, 0, UINT32_MAX);

exit:
    free(abs_path);
    amxc_string_clean(&abs_path_str);
    amxc_string_clean(&folder_path_str);
    return folder_path;
}

static int32_t get_file_size(const char* filename) {
    struct stat st;

    if(stat(filename, &st) == 0) {
        return (int32_t) st.st_size;
    } else {
        return -1;
    }
}

static int move_swu_file(const char* source, const char* dest) {
    amxc_string_t cmd_mv_file;
    char* dest_copy = NULL;
    char* base_dir_dest = NULL;
    char* dest_folder_path = NULL;
    char* source_copy = NULL;
    char* base_dir_src = NULL;
    int32_t size_src = 1;
    int32_t size_file = 1;
    int32_t size_decompressed = 1;
    int status = -1;
    int rv = -1;

    SAH_TRACEZ_WARNING(ME, "Downloading firmware");
    set_fwi_status(STATUS_DOWNLOADING, "");

    amxc_string_init(&cmd_mv_file, 0);
    when_str_empty(source, exit);
    when_false_trace(access(source, F_OK) == 0, exit, ERROR, "File '%s' doesn't exist.", source);

    source_copy = strdup(source);
    base_dir_src = dirname(source_copy);
    when_str_empty_trace(base_dir_src, exit, ERROR, "Source directory is empty.");
    when_false_trace(is_path_directory(base_dir_src) != 0, exit, ERROR, "Directory '%s' doesn't exist.", base_dir_src);

    when_str_empty(dest, exit);
    dest_folder_path = get_folder_path(dest);
    when_str_empty_trace(dest_folder_path, exit, ERROR, "Destination directory is empty. Check the config 'download_tmpfs_partition'");

    size_file = get_file_size(source);
    when_false_trace(size_file >= 0, exit, ERROR, "File size is not valid");
    size_file = (int32_t) (size_file / 1000000) + 1;

    size_decompressed = get_config_int32("upgrade_file_size_decompressed");

    if(strcmp(base_dir_src, dest_folder_path) != 0) {
        size_src = size_file + size_decompressed;
    } else {
        size_src = size_decompressed;
    }

    mount_tmpfs_partition(TMPFS_SCRIPT_USER, size_src);

    dest_copy = strdup(dest);
    base_dir_dest = dirname(dest_copy);
    when_str_empty_trace(base_dir_dest, exit, ERROR, "Destination directory is empty. Check the config 'download_tmpfs_partition'");
    when_false_trace(is_path_directory(base_dir_dest) != 0, exit, ERROR, "Directory '%s' doesn't exist. Check if the tmpfs was correctly mounted.", base_dir_dest);

    amxc_string_setf(&cmd_mv_file, "mv -f %s %s", source, dest);

    rv = system(amxc_string_get(&cmd_mv_file, 0));
    if(rv != 0) {
        SAH_TRACEZ_WARNING(ME, "Move file command returned error.");
    }
    when_false_trace(access(dest, F_OK) == 0, exit, ERROR, "File '%s' was not moved correctly.", dest);

    status = 0;
    SAH_TRACEZ_WARNING(ME, "Download succeeded");
exit:
    FREE(dest_folder_path);
    FREE(source_copy);
    FREE(dest_copy);
    amxc_string_clean(&cmd_mv_file);
    return status;
}

static int download_with_libfiletransfer(amxc_var_t* args, const char* path_swu) {
    int status = -1;
    amxc_var_t* callbackData = NULL;
    const char* tls_engine = NULL;
    const char* tls_client_cert = NULL;
    const char* tls_private_key = NULL;
    bool auto_activate = false;
    ftx_authentication_t auth_type = ftx_authentication_any;
    char* base_dir = NULL;

    when_null_trace(args, exit, ERROR, "Args is NULL");
    when_str_empty_trace(path_swu, exit, ERROR, "Path SWU is empty string");

    unmount_tmpfs_partition(TMPFS_SCRIPT_USER);
    mount_tmpfs_partition(TMPFS_SCRIPT_USER, get_config_int32("upgrade_file_size") + get_config_int32("upgrade_file_size_decompressed"));

    base_dir = get_folder_path(path_swu);
    when_str_empty_trace(base_dir, exit, ERROR, "Destination directory is empty. Check the config 'download_tmpfs_partition'");
    when_false_trace(is_path_directory(base_dir) != 0, exit, ERROR, "Directory '%s' doesn't exist. Check if the tmpfs was correctly mounted.", base_dir);

    SAH_TRACEZ_WARNING(ME, "Downloading firmware");
    set_fwi_status(STATUS_DOWNLOADING, "");

    auto_activate = GETP_BOOL(args, "parameters.AutoActivate");

    amxc_var_new(&callbackData);
    amxc_var_set_type(callbackData, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, callbackData, CB_AUTO_ACTIVATE, auto_activate);

    //Delete previous image
    delete_swu_file(path_swu);

    ftx_request_new(&ftx_req, ftx_request_type_download, download_cb);
    ftx_request_set_data(ftx_req, (void*) callbackData);
    ftx_request_set_url(ftx_req, GETP_CHAR(args, "parameters.URL"));

    if(!STRING_EMPTY(GETP_CHAR(args, "AuthenticationType"))) {
        if(strcmp(GETP_CHAR(args, "AuthenticationType"), "BASIC") == 0) {
            auth_type = ftx_authentication_basic;
        } else if(strcmp(GETP_CHAR(args, "AuthenticationType"), "DIGEST") == 0) {
            auth_type = ftx_authentication_digest;
        }
    }

    if(!STRING_EMPTY(GETP_CHAR(args, "parameters.Username")) && !STRING_EMPTY(GETP_CHAR(args, "parameters.Password"))) {
        ftx_request_set_credentials(ftx_req, auth_type, GETP_CHAR(args, "parameters.Username"), GETP_CHAR(args, "parameters.Password"));
    } else {
        ftx_request_set_credentials(ftx_req, auth_type, NULL, NULL);
    }

    if(!STRING_EMPTY(GETP_CHAR(args, "CACertificate"))) {
        if(is_path_directory(GETP_CHAR(args, "CACertificate")) != 0) {
            ftx_request_set_ca_certificates(ftx_req, NULL, GETP_CHAR(args, "CACertificate"));
        } else {
            ftx_request_set_ca_certificates(ftx_req, GETP_CHAR(args, "CACertificate"), NULL);
        }
    } else {
        ftx_request_set_ca_certificates(ftx_req, NULL, DEFAULT_PATH_SSL_CERTIFICATES);
    }

    if(!STRING_EMPTY(GETP_CHAR(args, "TLSEngine"))) {
        tls_engine = GETP_CHAR(args, "TLSEngine");
    }

    if(!STRING_EMPTY(GETP_CHAR(args, "TLSEngine")) && !STRING_EMPTY(GETP_CHAR(args, "TLSClientCertificate"))) {
        tls_client_cert = GETP_CHAR(args, "TLSClientCertificate");
    }

    if(!STRING_EMPTY(GETP_CHAR(args, "TLSEngine")) && !STRING_EMPTY(GETP_CHAR(args, "TLSPrivateKey"))) {
        tls_private_key = GETP_CHAR(args, "TLSPrivateKey");
    }

    ftx_request_set_client_certificates(ftx_req, tls_engine, tls_client_cert, tls_private_key);

    ftx_request_set_target_file(ftx_req, path_swu);
    ftx_request_set_max_transfer_time(ftx_req, 600);

    status = ftx_request_send(ftx_req);

exit:
    if(status != 0) {
        amxc_var_delete(&callbackData);
        ftx_request_delete(&ftx_req);
    }
    free(base_dir);
    return status;
}

int firmware_download(UNUSED const char* function_name,
                      amxc_var_t* args,
                      UNUSED amxc_var_t* ret) {
    int status = -1;
    bool download_error = true;
    amxc_string_t url;
    char* path_swu = NULL;
    bool auto_activate = false;

    amxc_string_init(&url, 0);

    when_true_trace(get_busy_status(), exit, WARNING, "Already an upgrade in progress");
    set_busy_status(true);

    path_swu = get_swu_path();
    when_str_empty_trace(path_swu, exit, ERROR, "Path SWU is empty string");
    when_str_empty_trace(GET_CHAR(args, "object"), exit, ERROR, "Object path is empty or NULL.");
    set_fwi_path(GET_CHAR(args, "object"));
    auto_activate = GETP_BOOL(args, "parameters.AutoActivate");

    download_error = false;
    amxc_string_setf(&url, "%s", GETP_CHAR(args, "parameters.URL"));
    //Check if the download comes from the filesystem
    if(amxc_string_search(&url, "file://", 0) >= 0) {
        amxc_string_replace(&url, "file://", "", UINT32_MAX);

        if(move_swu_file(amxc_string_get(&url, 0), path_swu) != 0) {
            download_error = true;
            goto exit;
        }

        status = firmware_flash(auto_activate);
        if(status != 0) {
            set_fwi_status(STATUS_VALIDFAILED, "Failed to flash the firmware.");
            set_busy_status(false);
            unmount_tmpfs_partition(TMPFS_SCRIPT_USER);
        }
        goto exit;
    }

    if(download_with_libfiletransfer(args, path_swu) != 0) {
        download_error = true;
        goto exit;
    }

    status = 0;

exit:
    if(download_error) {
        set_fwi_status(STATUS_DOWNFAILED, "Could not download the upgrade image");
        set_busy_status(false);
        //Delete the uploaded SWU file
        delete_swu_file(path_swu);
        unmount_tmpfs_partition(TMPFS_SCRIPT_USER);
    }
    amxc_string_clean(&url);
    free(path_swu);
    return status;
}

