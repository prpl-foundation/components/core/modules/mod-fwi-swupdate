/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>

#include "mod_common.h"
#include "flash_firmware.h"
#include "version_firmware.h"

#include <filetransfer/filetransfer.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#define ME "deviceinfo"
#define SWUPDATE_LOG "swu_log"

#define PREFIX_SCRIPT_NAME "flash_fwi_"

static amxp_subproc_t* proc_ctrl = NULL;
static amxc_string_t* str_stdout = NULL;
static int fd_stdout = 0;
static int fd_stderr = 0;
static int pid = 0;
static bool flash_ongoing = false;
static bool activate = false;

static void script_array_it_free(amxc_array_it_t* it) {
    free(it->data);
}

static void init_process_variables(void) {
    amxo_connection_remove(get_deviceinfo_parser(), fd_stdout);
    fd_stdout = 0;
    fd_stderr = 0;
    pid = 0;
    amxc_string_delete(&str_stdout);
    str_stdout = NULL;
    amxp_subproc_delete(&proc_ctrl);
    proc_ctrl = NULL;
}

/**
   @brief
   Create the swupdate command.

   @param[in] auto_activate Autoactivate parameter.
   @param[out] cmd String with the command.
 */
static int create_swupdate_command(bool auto_activate, amxc_string_t* cmd) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    rootfs_type_t fw_mode = (get_fwi_index() == 1) ? rootfs_active : rootfs_inactive;
    const char* image_type = (fw_mode == rootfs_inactive) ? "rescue" : "nominal";
    const char* flash_type = auto_activate ? "full" : "noactivate";
    char* swu_file = get_swu_path();

    when_null_trace(cmd, exit, ERROR, "Array is not initialised.");

    amxc_string_reset(cmd);
    amxc_string_appendf(cmd, "TMPDIR=%s swupdate -v -i %s -k %s -e %s,%s", get_config("download_tmpfs_partition"), swu_file, get_config("upgrade_key"), image_type, flash_type);
    SAH_TRACEZ_WARNING(ME, "Command used to flash: swupdate -v -i %s -k %s -e %s,%s", swu_file, get_config("upgrade_key"), image_type, flash_type);

    rv = 0;
exit:
    free(swu_file);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
   @brief
   Kill a specific script process.

   @return
   Return of the kill command.
 */
static int kill_script(void) {
    int rv = -1;
    int status = -1;
    amxp_subproc_t* proc = NULL;
    amxc_string_t* pid_str = NULL;

    when_false_trace(pid > 0, exit, WARNING, "The process PID is less or equal 0.");

    amxc_string_new(&pid_str, 0);
    amxc_string_setf(pid_str, "%d", pid);

    rv = amxp_subproc_new(&proc);
    when_failed_trace(rv, exit, ERROR, "Failed to initialize the subprocess to kill the proccess.");

    rv = amxp_subproc_start_wait(proc, 1000, (char*) "kill", (char*) "-9", (char*) amxc_string_get(pid_str, 0), NULL);
    when_failed_trace(rv, exit, ERROR, "Failed to run the kill command.");

    status = 0;

exit:
    amxp_subproc_delete(&proc);
    amxc_string_delete(&pid_str);
    return status;
}

/**
   @brief
   Delete the script according to the firmware image.
 */
static void delete_script(void) {
    SAH_TRACEZ_IN(ME);
    amxc_string_t* script_file = NULL;
    const uint32_t firmware_number = get_fwi_index();

    when_true_trace(firmware_number <= 0, exit, ERROR, "Failed to get the FirmwareImage index.");

    amxc_string_new(&script_file, 0);
    amxc_string_setf(script_file, "/tmp/%s%u.sh", PREFIX_SCRIPT_NAME, firmware_number);
    delete_file(amxc_string_get(script_file, 0));
    amxc_string_delete(&script_file);
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

/**
   @brief
   Create the shell script.

   @param[in] cmd String with the command.
   @param[in] script_file Script file name.
   @param[out] sh_script Array used in the process.

   @return
   0 if it passes.
 */
static int create_sh_flash_script(amxc_string_t* cmd, amxc_array_t* sh_script) {
    int rv = -1;
    FILE* p_sh_file = NULL;
    amxc_string_t script_file;
    const uint32_t firmware_number = get_fwi_index();

    amxc_string_init(&script_file, 1);

    when_null_trace(cmd, exit, ERROR, "Command is NULL.");
    when_null_trace(sh_script, exit, ERROR, "Array is not initialised.");
    when_true_trace(firmware_number <= 0, exit, ERROR, "Failed to get the FirmwareImage index.");

    amxc_string_setf(&script_file, "/tmp/%s%u.sh", PREFIX_SCRIPT_NAME, firmware_number);

    amxc_array_append_data(sh_script, strdup("sh"));
    amxc_array_append_data(sh_script, strdup(amxc_string_get(&script_file, 0)));
    // Delete the shell script with the firmware number in order to clean up the environment
    delete_script();
    p_sh_file = fopen(amxc_string_get(&script_file, 0), "w");
    when_null_trace(p_sh_file, exit, ERROR, "Unable to create the shell script.")

    fprintf(p_sh_file, "#!/bin/sh\n\n");
    fprintf(p_sh_file, "set +x\n");
    fprintf(p_sh_file, "%s\n", amxc_string_get(cmd, 0));
    fprintf(p_sh_file, "echo \"%s$?\"\n", STRING_SCRIPT_RESULT);
    fprintf(p_sh_file, "echo \"%s\"\n", STRING_END_OF_SCRIPT);
    fprintf(p_sh_file, "sleep 10\n");

    fclose(p_sh_file);
    chmod(amxc_string_get(&script_file, 0), 0550);

    rv = 0;

exit:
    amxc_string_clean(&script_file);
    return rv;
}

/**
   @brief
   Parse log from the standard output string.

   @return
   0 if the swupdate ran succesfully, -1 if it's still running.
 */
static int parse_log(void) {
    int status = -1;
    amxc_llist_t stdout_lines;
    amxc_string_t* line = NULL;
    int num_of_lines = 0;
    bool script_finished = false;

    amxc_llist_init(&stdout_lines);

    when_null(str_stdout, exit);
    when_str_empty(amxc_string_get(str_stdout, 0), exit);

    split_string_lines(str_stdout, &stdout_lines);
    //TODO Use this when issue 65 of libamxc is solved
    // amxc_string_split_to_llist(str_stdout, &stdout_lines, '\n');
    num_of_lines = amxc_llist_size(&stdout_lines);
    when_false(num_of_lines, exit);

    for(int line_num = 0; line_num < num_of_lines; line_num++) {
        line = amxc_string_get_from_llist(&stdout_lines, line_num);
        if(amxc_string_search(line, STRING_END_OF_SCRIPT, 0) >= 0) {
            script_finished = true;
            SAH_TRACEZ_WARNING(SWUPDATE_LOG, "swupdate command ended");
            continue;
        }

        if(amxc_string_search(line, STRING_SCRIPT_RESULT, 0) >= 0) {
            amxc_string_replace(line, STRING_SCRIPT_RESULT, "", UINT32_MAX);
            amxc_string_replace(line, " ", "", UINT32_MAX);
            amxc_string_replace(line, "\n", "", UINT32_MAX);
            status = atoi(amxc_string_get(line, 0));
            continue;
        }

        SAH_TRACEZ_WARNING_WITHOUT_SOURCE(SWUPDATE_LOG, "%s", amxc_string_get(line, 0));
    }
    amxc_string_reset(str_stdout);

    if(script_finished && (status == -1)) {
        status = 1;
    }

exit:
    amxc_llist_clean(&stdout_lines, amxc_string_list_it_free);
    return status;
}

void flash_fd_cb(UNUSED int fd, UNUSED void* priv) {
    int rv = -1;
    int status = -1;

    if(str_stdout == NULL) {
        amxc_string_new(&str_stdout, 0);
    }

    rv = amxp_subproc_wait(proc_ctrl, 1);
    if(rv == 1) {
        read_from_fd(fd_stdout, str_stdout);
        read_from_fd(fd_stderr, str_stdout);
        status = parse_log();
    } else {
        status = 1;
    }

    if(status >= 0) {
        amxp_subproc_kill(proc_ctrl, SIGTERM);
        amxp_subproc_wait(proc_ctrl, 50);
        kill_script();
        init_process_variables();
        delete_script();

        SAH_TRACEZ_WARNING(ME, "Flashing finished with status: %d", status);
        flash_ongoing = false;
        set_busy_status(false);
        unmount_tmpfs_partition(TMPFS_SCRIPT_USER);

        if(status > 0) {
            if(activate) {
                set_fwi_status(STATUS_ACTIVFAILED, "swupdate failed to upgrade");
            } else {
                set_fwi_status(STATUS_VALIDFAILED, "swupdate failed to upgrade");
            }
        } else {
            if(activate) {
                set_fwi_status(STATUS_ACTIVE, "");
                reboot_board();
            } else {
                set_fwi_status(STATUS_AVAILABLE, "");
            }
        }
    }
}

int firmware_flash(bool auto_activate) {
    int rv = -1;
    int status = -1;
    amxc_array_t sh_script;
    amxc_string_t* cmd = NULL;
    char* swu_file = get_swu_path();

    amxc_string_new(&cmd, 0);
    amxc_array_init(&sh_script, 1);

    activate = auto_activate;
    when_true_trace(flash_ongoing, exit, ERROR, "There is a flashing process ongoing.");

    init_process_variables();

    rv = create_swupdate_command(auto_activate, cmd);
    when_failed_trace(rv, exit, ERROR, "Failed to create the swupdate command.");

    rv = create_sh_flash_script(cmd, &sh_script);
    when_failed_trace(rv, exit, ERROR, "Failed to create the shell script to flash the image.");

    rv = amxp_subproc_new(&proc_ctrl);
    when_failed_trace(rv, exit, ERROR, "Failed to initialize the subprocess.");

    fd_stdout = amxp_subproc_open_fd(proc_ctrl, STDOUT_FILENO);
    fd_stderr = amxp_subproc_open_fd(proc_ctrl, STDERR_FILENO);
    if((fd_stdout == -1) || (fd_stderr == -1)) {
        SAH_TRACEZ_ERROR(ME, "Failed to open file descriptor.");
        goto exit;
    }
    SAH_TRACEZ_WARNING(ME, "swupdate script fd = %d.", fd_stdout);

    rv = amxp_subproc_astart(proc_ctrl, &sh_script);
    when_failed_trace(rv, exit, ERROR, "Failed to start swupdate script.");

    pid = (int) amxp_subproc_get_pid(proc_ctrl);
    when_true_trace(pid <= 0, exit, ERROR, "Failed to get the swupdate script's PID.");
    SAH_TRACEZ_WARNING(ME, "swupdate script started with PID = %d.", pid);

    rv = amxo_connection_add(get_deviceinfo_parser(), fd_stdout, flash_fd_cb, NULL, AMXO_CUSTOM, NULL);
    when_failed_trace(rv, exit, ERROR, "Failed to add the fd to be watched.");

    SAH_TRACEZ_WARNING(ME, "Attempt to flash using image: %s", swu_file);
    set_fwi_status(STATUS_VALIDATING, "");
    flash_ongoing = true;
    status = 0;

exit:
    if(status != 0) {
        init_process_variables();
    }
    free(swu_file);
    amxc_string_delete(&cmd);
    amxc_array_clean(&sh_script, script_array_it_free);
    return status;
}
