/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "mod_common.h"
#include "download_firmware.h"
#include "activate_firmware.h"
#include "flash_firmware.h"
#include "version_firmware.h"

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#define ME "deviceinfo"

static amxm_module_t* mod = NULL;

static AMXM_CONSTRUCTOR fwi_wrt_start(void) {
    int rv = -1;
    amxm_shared_object_t* so = amxm_so_get_current();

    set_busy_status(false);
    SAH_TRACEZ_INFO(ME, "Start wrt fwi module");

    rv = amxm_module_register(&mod, so, MOD_FWI_CTRL);
    when_failed_trace(rv, error, ERROR, "Failed to register the module %s", MOD_FWI_CTRL);

    rv = amxm_module_add_function(mod, "download", firmware_download);
    when_failed_trace(rv, error, ERROR, "Failed to add the function %s from the module %s", "download", MOD_FWI_CTRL);

    rv = amxm_module_add_function(mod, "activate", firmware_activate);
    when_failed_trace(rv, error, ERROR, "Failed to add the function %s from the module %s", "activate", MOD_FWI_CTRL);

    rv = amxm_module_add_function(mod, "info", get_firmware_info);
    when_failed_trace(rv, error, ERROR, "Failed to add the function %s from the module %s", "info", MOD_FWI_CTRL);

    rv = amxm_module_add_function(mod, "setparser", set_parser);
    when_failed_trace(rv, error, ERROR, "Failed to add the function %s from the module %s", "setparser", MOD_FWI_CTRL);

    rv = amxm_module_add_function(mod, "setdm", set_dm);
    when_failed_trace(rv, error, ERROR, "Failed to add the function %s from the module %s", "setdm", MOD_FWI_CTRL);

error:
    return rv;
}

static AMXM_DESTRUCTOR fwi_wrt_stop(void) {
    SAH_TRACEZ_INFO(ME, "Stop wrt fwi module");
    clean_filetransfer_ctx();
    clean_fwi_path();
    return 0;
}
