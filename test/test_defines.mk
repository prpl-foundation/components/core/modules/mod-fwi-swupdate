MACHINE = $(shell $(CC) -dumpmachine)

SRCDIR += $(realpath ../../src)
OBJDIR = $(realpath ../../output/$(MACHINE)/coverage)
INCDIR += $(realpath ../../include_priv)
# MOCK_SRCDIR
COMMON_SRCDIR += $(realpath ../common/)
# MOCK_INCDIR = $(realpath ../common/)
INCDIR += $(realpath ../common_includes/)

HEADERS = $(wildcard $(addsuffix /*.h,$(INCDIR)))
SOURCES = $(wildcard $(SRCDIR)/*.c)
SOURCES += $(wildcard $(COMMON_SRCDIR)/*.c)
SOURCES += $(wildcard $(MOCK_SRCDIR)/*.c)

WRAP_FUNC=-Wl,--wrap=
MOCK_WRAP = access \
            remove

CFLAGS += -Werror -Wall -Wextra -Wno-attributes\
          --std=gnu99 -g3 -Wmissing-declarations \
		  $(addprefix -I ,$(INCDIR)) -I$(OBJDIR)/.. \
		  -g $(addprefix $(WRAP_FUNC),$(MOCK_WRAP)) \
          -fkeep-inline-functions -fkeep-static-functions \
		  -Wno-format-nonliteral \
		  $(shell pkg-config --cflags cmocka) -pthread \
		  -D_GNU_SOURCE -DSAHTRACES_ENABLED -DSAHTRACES_LEVEL=500

LDFLAGS += -fkeep-inline-functions -fkeep-static-functions \
		   -g $(addprefix $(WRAP_FUNC),$(MOCK_WRAP)) \
           $(shell pkg-config --libs cmocka) \
		   -lamxc -lamxp -lamxd -lamxo -lamxb -lamxm \
		   -lsahtrace -ldl -lpthread -lfiletransfer -levent
