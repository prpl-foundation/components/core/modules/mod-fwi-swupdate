/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>
#include <sys/stat.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxb/amxb.h>
#include <amxb/amxb_register.h>
#include <amxb/amxb_connect.h>
#include <amxo/amxo.h>
#include <filetransfer/filetransfer.h>

#include "test_module.h"
#include "mod_common.h"
#include "activate_firmware.h"
#include "download_firmware.h"
#include "flash_firmware.h"
#include "dummy_backend.h"
#include "amxp_mock.h"
#include "test_common.h"

#define TMPFS_PATH "./downloads"

static const char* mock_odl_defs = "../mock_odl/mock.odl";
static const char* url_file = "http://proof.ovh.net/files/1Mb.dat";
static const char* data_file = "../test_data/test.swu";
static const char* copy_data_file = "../test_data/copy_test.swu";
static const char* flash_swu_file = TMPFS_PATH "/flash.swu";
static const char* flash_swu_file_copy = TMPFS_PATH "_copy/flash.swu";
static const char* tmpfs_path = TMPFS_PATH;
static const char* tmpfs_path_copy = TMPFS_PATH "_copy";

static amxd_dm_t dm;
static amxo_parser_t parser;
static amxb_bus_ctx_t* bus_ctx = NULL;
static amxc_var_t args;
static amxc_var_t ret;
static amxc_string_t string;

static long int file_size(const char* file) {
    long int res;
    FILE* fp = fopen(file, "r");

    assert_non_null(fp);
    fseek(fp, 0L, SEEK_END);
    res = ftell(fp);
    fclose(fp);

    return res;
}

static void check_for_file(const char* file, long int size) {
    assert_int_equal(access(file, F_OK), 0);
    assert_int_equal(file_size(file), size);
}


static const char* get_data_file(const char* file) {
    char* path = realpath(file, NULL);
    amxc_string_setf(&string, "file://%s", path);
    free(path);
    return amxc_string_get(&string, 0);
}

static void clean_variables() {
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

static int download(const char* url, const bool auto_activate, const char* username, const char* password,
                    const char* filesize, const char* check_sum_algorithm, const char* check_sum, bool wait_for_libtransfer) {
    amxc_var_t* params = NULL;
    int rv = 0;

    clean_variables();
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "object", "DeviceInfo.FirmwareImage.1.");
    params = amxc_var_add_key(amxc_htable_t, &args, "parameters", NULL);
    if(url != NULL) {
        amxc_var_add_key(cstring_t, params, "URL", url);
    }
    amxc_var_add_key(bool, params, "AutoActivate", auto_activate);
    if(username != NULL) {
        amxc_var_add_key(cstring_t, params, "Username", username);
    }
    if(password != NULL) {
        amxc_var_add_key(cstring_t, params, "Password", password);
    }
    if(filesize != NULL) {
        amxc_var_add_key(cstring_t, params, "FileSize", filesize);
    }
    if(check_sum_algorithm != NULL) {
        amxc_var_add_key(cstring_t, params, "CheckSumAlgorithm", check_sum_algorithm);
    }
    if(check_sum != NULL) {
        amxc_var_add_key(cstring_t, params, "CheckSum", check_sum);
    }

    rv = firmware_download("download", &args, &ret);
    assert_int_equal(rv, 0);
    if(wait_for_libtransfer) {
        event_loop_start();
    }
    handle_events();
    flash_fd_cb(0, NULL);

    return rv;
}

static int activate(const uint32_t start, const uint32_t end, const char* mode, const char* user_message, const uint32_t max_retries) {
    amxc_var_t* params = NULL;
    int rv = 0;

    clean_variables();
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "object", "DeviceInfo.FirmwareImage.1.");
    params = amxc_var_add_key(amxc_htable_t, &args, "parameters", NULL);
    amxc_var_add_key(uint32_t, params, "Start", start);
    amxc_var_add_key(uint32_t, params, "End", end);
    if(mode != NULL) {
        amxc_var_add_key(cstring_t, params, "Mode", mode);
    }
    if(user_message != NULL) {
        amxc_var_add_key(cstring_t, params, "UserMessage", user_message);
    }
    amxc_var_add_key(uint32_t, params, "MaxRetries", max_retries);

    rv = firmware_activate("activate", &args, &ret);

    return rv;
}

int test_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    amxc_string_init(&string, 0);
    assert_int_equal(event_loop_init(), 0);
    init_filetransfer_ctx(fd_set_cb);

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);
    assert_int_equal(test_register_dummy_be(), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    amxo_connection_add(&parser,
                        amxb_get_fd(bus_ctx),
                        connection_read,
                        "dummy:/tmp/dummy.sock",
                        AMXO_BUS,
                        bus_ctx);
    amxb_register(bus_ctx, &dm);

    assert_int_equal(amxo_parser_parse_file(&parser, mock_odl_defs, root_obj), 0);

    // empty signal handler for 'amxp_slot_connect'
    amxp_sigmngr_add_signal(NULL, "connection-deleted");
    handle_events();

    return 0;
}

int test_teardown(UNUSED void** state) {
    amxp_sigmngr_emit_signal(&(dm.sigmngr), "app:stop", NULL);
    handle_events();

    amxb_free(&bus_ctx);
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);
    clean_variables();

    amxo_resolver_import_close_all();
    clean_filetransfer_ctx();
    event_loop_clean();
    amxc_string_clean(&string);
    handle_events();
    test_unregister_dummy_be();
    return 0;
}

void test_functions(UNUSED void** state) {
    amxc_string_t* file_content = NULL;
    system("rm -rf "TMPFS_PATH "_copy");

    //Add parser
    assert_int_equal(set_parser("setparser", (amxc_var_t*) &parser, NULL), 0);

    //Add dm
    assert_int_equal(set_dm("setdm", (amxc_var_t*) &dm, NULL), 0);

    //Download file from the internet without autoactivate
    assert_int_equal(download(url_file, false, NULL, NULL, NULL, NULL, NULL, true), 0);
    assert_int_not_equal(is_path_directory(tmpfs_path_copy), 0);
    check_for_file(flash_swu_file_copy, 1048576);
    assert_int_equal(is_path_directory(tmpfs_path), 0);
    system("rm -rf "TMPFS_PATH "_copy");
    assert_int_equal(is_path_directory(tmpfs_path_copy), 0);

    //Download file from the internet with autoactivate
    set_auto_activate(true);
    assert_int_equal(download(url_file, true, NULL, NULL, NULL, NULL, NULL, true), 0);
    assert_int_not_equal(is_path_directory(tmpfs_path_copy), 0);
    check_for_file(flash_swu_file_copy, 1048576);
    assert_int_equal(is_path_directory(tmpfs_path), 0);
    system("rm -rf "TMPFS_PATH "_copy");
    assert_int_equal(is_path_directory(tmpfs_path_copy), 0);

    //Download file from the testdata without autoactivate
    copy_file(data_file, copy_data_file);
    assert_int_equal(download(get_data_file(copy_data_file), false, NULL, NULL, NULL, NULL, NULL, false), 0);
    assert_int_not_equal(access(copy_data_file, F_OK), 0);
    assert_int_not_equal(is_path_directory(tmpfs_path_copy), 0);
    check_for_file(flash_swu_file_copy, 19);
    assert_int_equal(is_path_directory(tmpfs_path), 0);
    amxc_string_new(&file_content, 0);
    read_from_file(flash_swu_file_copy, file_content);
    assert_string_equal(amxc_string_get(file_content, 0), "This is just a test");
    amxc_string_delete(&file_content);
    system("rm -rf "TMPFS_PATH "_copy");
    assert_int_equal(is_path_directory(tmpfs_path_copy), 0);

    //Activate
    set_auto_activate(false);
    assert_int_equal(activate(0, 10, "Immediately", NULL, 1), 0);

    //Download file from the testdata with autoactivate
    copy_file(data_file, copy_data_file);
    set_auto_activate(true);
    assert_int_equal(download(get_data_file(copy_data_file), true, NULL, NULL, NULL, NULL, NULL, false), 0);
    assert_int_not_equal(access(copy_data_file, F_OK), 0);
    assert_int_not_equal(is_path_directory(tmpfs_path_copy), 0);
    check_for_file(flash_swu_file_copy, 19);
    assert_int_equal(is_path_directory(tmpfs_path), 0);
    amxc_string_new(&file_content, 0);
    read_from_file(flash_swu_file_copy, file_content);
    assert_string_equal(amxc_string_get(file_content, 0), "This is just a test");
    amxc_string_delete(&file_content);
    system("rm -rf "TMPFS_PATH "_copy");
    assert_int_equal(is_path_directory(tmpfs_path_copy), 0);

    //Download flash.swu file from the testdata without autoactivate
    system("mkdir -p " TMPFS_PATH);
    copy_file(data_file, flash_swu_file);
    assert_int_equal(download(get_data_file(flash_swu_file), false, NULL, NULL, NULL, NULL, NULL, false), 0);
    assert_int_not_equal(is_path_directory(tmpfs_path_copy), 0);
    check_for_file(flash_swu_file_copy, 19);
    assert_int_equal(is_path_directory(tmpfs_path), 0);
    amxc_string_new(&file_content, 0);
    read_from_file(flash_swu_file_copy, file_content);
    assert_string_equal(amxc_string_get(file_content, 0), "This is just a test");
    amxc_string_delete(&file_content);
    system("rm -rf "TMPFS_PATH "_copy");
    assert_int_equal(is_path_directory(tmpfs_path_copy), 0);

    //Activate
    set_auto_activate(false);
    assert_int_equal(activate(0, 10, "Immediately", NULL, 1), 0);

    //Download flash.swu file from the testdata with autoactivate
    system("mkdir -p " TMPFS_PATH);
    copy_file(data_file, flash_swu_file);
    set_auto_activate(true);
    assert_int_equal(download(get_data_file(flash_swu_file), true, NULL, NULL, NULL, NULL, NULL, false), 0);
    assert_int_not_equal(is_path_directory(tmpfs_path_copy), 0);
    check_for_file(flash_swu_file_copy, 19);
    assert_int_equal(is_path_directory(tmpfs_path), 0);
    amxc_string_new(&file_content, 0);
    read_from_file(flash_swu_file_copy, file_content);
    assert_string_equal(amxc_string_get(file_content, 0), "This is just a test");
    amxc_string_delete(&file_content);
    system("rm -rf "TMPFS_PATH "_copy");
    assert_int_equal(is_path_directory(tmpfs_path_copy), 0);
}