/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxo/amxo.h>
#include <amxb/amxb.h>

#include "amxp_mock.h"

#define PROC_LIST_SIZE 20

typedef struct {
    amxp_subproc_t* subproc;
} process_t;

amxc_var_t* proc_map = NULL;
amxp_subproc_t* subproc_opened_fd = NULL;
static int swupdate_status = 0;
static int proc_status = 0;
static bool swupdate_auto_activate = false;
static int read_fd_stdout = -1;
static int read_fd_stderr = -1;

process_t proc_list[PROC_LIST_SIZE];

static void init_proc_list(void) {
    for(int i = 1; i < PROC_LIST_SIZE; i++) {
        proc_list[i].subproc = NULL;
    }
}

int amxp_subproc_new(amxp_subproc_t** subproc) {
    *subproc = calloc(1, sizeof(amxp_subproc_t));
    return 0;
}

int amxp_subproc_delete(amxp_subproc_t** subproc) {
    int rv = -1;
    const amxc_htable_t* ht_proc_map = NULL;
    int index = -1;
    amxc_string_t index_str;

    amxc_string_init(&index_str, 0);

    when_null(*subproc, exit);
    when_null(proc_map, exit);

    for(int i = 1; i < PROC_LIST_SIZE; i++) {
        if(proc_list[i].subproc != NULL) {
            if(proc_list[i].subproc == *subproc) {
                index = i;
                amxc_string_setf(&index_str, "%d", index);
                break;
            }
        }
    }
    when_true(index == -1, exit);

    amxc_var_t* tmp = GET_ARG(proc_map, amxc_string_get(&index_str, 0));
    when_null(tmp, exit);
    amxc_var_delete(&tmp);

    ht_proc_map = amxc_var_constcast(amxc_htable_t, proc_map);
    if(amxc_htable_size(ht_proc_map) == 0) {
        amxc_var_delete(&proc_map);
        proc_map = NULL;
    }

    if(proc_list[index].subproc == subproc_opened_fd) {
        subproc_opened_fd = NULL;
        if(access("fd_test", F_OK) == 0) {
            remove("fd_test");
        }
        swupdate_status = 0;
        proc_status = 0;
    }
    free(proc_list[index].subproc);
    proc_list[index].subproc = NULL;

    rv = 0;
exit:
    amxc_string_clean(&index_str);
    return rv;
}

int amxp_subproc_open_fd(amxp_subproc_t* subproc, int requested) {
    int fd = -1;
    int read_fd = -1;
    amxc_string_t pid_line;

    amxc_string_init(&pid_line, 0);

    when_null(subproc, exit);

    if(requested == STDOUT_FILENO) {
        if(access("fd_test_stdout", F_OK) == 0) {
            remove("fd_test_stdout");
        }

        fd = open("fd_test_stdout", O_RDWR | O_CREAT | O_TRUNC, 0666);
        assert_true(fd > 0);

        amxc_string_setf(&pid_line, "+++Script Result: %d", swupdate_status);
        write(fd, "Something", 9);
        write(fd, "\n", 1);
        write(fd, amxc_string_get(&pid_line, 0), amxc_string_text_length(&pid_line));
        write(fd, "\n", 1);
        write(fd, "+++Finished script+++", 21);
        write(fd, "\n", 1);

        close(fd);

        read_fd = open("fd_test_stdout", O_RDONLY);
        read_fd_stdout = read_fd;
    } else {
        if(access("fd_test_stderr", F_OK) == 0) {
            remove("fd_test_stderr");
        }

        fd = open("fd_test_stderr", O_RDWR | O_CREAT | O_TRUNC, 0666);
        assert_true(fd > 0);
        write(fd, "\n", 1);
        close(fd);
        read_fd = open("fd_test_stderr", O_RDONLY);
        read_fd_stderr = read_fd;
    }

    assert_true(read_fd > 0);
    subproc_opened_fd = subproc;
    proc_status = 1;

exit:
    amxc_string_clean(&pid_line);
    return read_fd;
}

static void map_process(amxp_subproc_t* subproc, char* cmd) {
    int index = -1;
    amxc_string_t index_str;

    assert_non_null(cmd);

    amxc_string_init(&index_str, 0);

    if(proc_map == NULL) {
        amxc_var_new(&proc_map);
        amxc_var_set_type(proc_map, AMXC_VAR_ID_HTABLE);
        init_proc_list();
    }

    for(int i = 1; i < PROC_LIST_SIZE; i++) {
        if(proc_list[i].subproc == subproc) {
            index = i;
            amxc_string_setf(&index_str, "%d", index);
            break;
        }
    }

    if(index == -1) {
        for(int i = 1; i < PROC_LIST_SIZE; i++) {
            if(proc_list[i].subproc == NULL) {
                index = i;
                amxc_string_setf(&index_str, "%d", index);
                break;
            }
        }
    } else {
        amxc_var_t* tmp = GET_ARG(proc_map, amxc_string_get(&index_str, 0));
        assert_non_null(tmp);
        amxc_var_delete(&tmp);
    }

    assert_int_not_equal(index, -1);
    proc_list[index].subproc = subproc;

    amxc_var_add_key(cstring_t, proc_map, amxc_string_get(&index_str, 0), cmd);
    amxc_string_clean(&index_str);
}


int amxp_subproc_start(amxp_subproc_t* subproc, char* cmd, ...) {
    int rv = -1;
    va_list ap;
    int argc = 0;
    amxc_string_t* str = NULL;
    amxc_string_t tmp_string;

    amxc_string_new(&str, 0);
    amxc_string_init(&tmp_string, 0);

    when_null(subproc, exit);
    when_null(cmd, exit);

    va_start(ap, cmd);
    while(va_arg(ap, char*)) {
        argc++;
    }
    va_end(ap);

    va_start(ap, cmd);
    // By convention, the first argument is always the executable name
    amxc_string_appendf(str, "%s", cmd);
    for(int i = 1; i <= argc; i++) {
        amxc_string_appendf(str, " %s", va_arg(ap, char*));
    }
    va_end(ap);

    if(amxc_string_search(str, "tmpfs_download_mount", 0) >= 0) {
        system("mkdir -p ./downloads/");
    } else if(amxc_string_search(str, "tmpfs_download_unmount", 0) >= 0) {
        system("mkdir -p ./downloads_copy/");
        system("cp -rf ./downloads/* ./downloads_copy");
        system("rm -rf ./downloads/");
    }
    map_process(subproc, (char*) amxc_string_get(str, 0));

    rv = 0;
exit:
    amxc_string_delete(&str);
    amxc_string_clean(&tmp_string);
    return rv;
}


int amxp_subproc_start_wait(amxp_subproc_t* subproc, UNUSED int timeout_msec, char* cmd, ...) {
    int rv = -1;
    va_list ap;
    int argc = 0;
    amxc_string_t* str = NULL;
    amxc_string_t tmp_string;

    amxc_string_new(&str, 0);
    amxc_string_init(&tmp_string, 0);

    when_null(subproc, exit);
    when_null(cmd, exit);

    va_start(ap, cmd);
    while(va_arg(ap, char*)) {
        argc++;
    }
    va_end(ap);

    va_start(ap, cmd);
    // By convention, the first argument is always the executable name
    amxc_string_appendf(str, "%s", cmd);
    for(int i = 1; i <= argc; i++) {
        amxc_string_appendf(str, " %s", va_arg(ap, char*));
    }
    va_end(ap);

    map_process(subproc, (char*) amxc_string_get(str, 0));

    rv = 0;
exit:
    amxc_string_delete(&str);
    amxc_string_clean(&tmp_string);
    return rv;
}

int amxp_subproc_astart(amxp_subproc_t* const subproc, amxc_array_t* cmd) {
    int rv = -1;
    amxc_string_t* str = NULL;

    amxc_string_new(&str, 0);

    when_null(subproc, exit);
    when_null(cmd, exit);

    for(size_t i = 0; i < amxc_array_capacity(cmd); i++) {
        if(amxc_array_get_data_at(cmd, i) == NULL) {
            break;
        }
        amxc_string_appendf(str, "%s ", (char*) amxc_array_get_data_at(cmd, i));
    }

    map_process(subproc, (char*) amxc_string_get(str, 0));

    rv = 0;
exit:
    amxc_string_delete(&str);
    return rv;
}

int amxp_subproc_kill(const amxp_subproc_t* const subproc, UNUSED const int sig) {
    return amxp_subproc_delete((amxp_subproc_t**) &subproc);
}

int amxp_subproc_wait(amxp_subproc_t* subproc, UNUSED int timeout_msec) {
    int rv = 0;

    when_null(subproc, exit);
    when_null(proc_map, exit);

    rv = proc_status;

exit:
    return rv;
}

int amxp_subproc_get_exitstatus(UNUSED amxp_subproc_t* subproc) {
    return 0;
}

pid_t amxp_subproc_get_pid(const amxp_subproc_t* const subproc) {
    int index = -1;

    when_null(proc_map, exit);

    for(int i = 1; i < PROC_LIST_SIZE; i++) {
        if(proc_list[i].subproc != NULL) {
            if(proc_list[i].subproc == subproc) {
                index = i;
                break;
            }
        }
    }

exit:
    return index;
}

amxp_subproc_t* amxp_subproc_find(const int pid) {
    amxp_subproc_t* ret = NULL;

    when_true(pid < 0, exit);
    when_true(pid >= PROC_LIST_SIZE, exit);

    ret = proc_list[pid].subproc;
exit:
    return ret;
}

void amxp_mock_cleanup(void) {
    for(int i = 1; i < PROC_LIST_SIZE; i++) {
        if(proc_list[i].subproc != NULL) {
            free(proc_list[i].subproc);
        }
    }
    amxc_var_delete(&proc_map);
}

int processes_running(void) {
    int size = 0;
    const amxc_htable_t* ht_proc_map = NULL;

    when_null(proc_map, exit);
    ht_proc_map = amxc_var_constcast(amxc_htable_t, proc_map);
    when_null(ht_proc_map, exit);

    size = (int) amxc_htable_size(ht_proc_map);
exit:
    return size;
}

void amxp_mock_dump(void) {
    amxc_var_dump(proc_map, STDOUT_FILENO);
    fflush(stdout);
}

void set_auto_activate(bool p_auto_activate) {
    swupdate_auto_activate = p_auto_activate;
}

void set_swupdate_status(int status) {
    swupdate_status = status;
}

void set_proc_status(int status) {
    proc_status = status;
}