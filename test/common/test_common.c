/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <string.h>
#include <regex.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <event2/event.h>

#include <amxc/amxc_macros.h>

#include "test_common.h"
#include <filetransfer/filetransfer.h>
#include "mod_common.h"

static struct event_base* base = NULL;
static struct event* ev_fd = NULL;

void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

void dump_object(amxd_object_t* object, int index) {
    amxd_param_t* param = NULL;
    amxd_object_t* tmp_obj = NULL;
    amxc_var_t* tmp_var = NULL;
    cstring_t obj_name = NULL;
    cstring_t param_name = NULL;
    cstring_t param_value = NULL;
    index++;

    if(object == NULL) {
        printf("The object is NULL.\n");
    } else {
        amxc_llist_for_each(it, (&object->parameters)) {
            param = amxc_llist_it_get_data(it, amxd_param_t, it);
            param_name = (cstring_t) amxd_param_get_name(param);
            tmp_var = (amxc_var_t*) amxd_object_get_param_value(object, param_name);
            param_value = amxc_var_dyncast(cstring_t, tmp_var);
            printf("param_name[%d]: %s = %s\n", index, param_name, param_value);
            free(param_value);
            param = NULL;
        }
        amxc_llist_for_each(it, (&object->objects)) {
            tmp_obj = amxc_llist_it_get_data(it, amxd_object_t, it);
            obj_name = (cstring_t) amxd_object_get_name(tmp_obj, AMXD_OBJECT_INDEXED);
            printf("obj_name[%d]: %s\n", index, obj_name);
            dump_object(tmp_obj, index);
            tmp_obj = NULL;
        }
        amxc_llist_for_each(it, (&object->instances)) {
            tmp_obj = amxc_llist_it_get_data(it, amxd_object_t, it);
            obj_name = (cstring_t) amxd_object_get_name(tmp_obj, AMXD_OBJECT_INDEXED);
            printf("instance_name[%d]: %s\n", index, obj_name);
            dump_object(tmp_obj, index);
            tmp_obj = NULL;
        }
    }
    fflush(stdout);
}

/**
   @brief
   Fetch the instance number from the URI.

   @param[in] uri URI.

   @return
   0 when it fails.
 */
uint32_t parse_instance_index_from_uri(cstring_t uri) {
    uint32_t instance_index = 0;
    amxc_string_t* string_uri = NULL;
    amxc_string_t* extracted_string = NULL;
    amxc_string_t* partial_string = NULL;
    int rv;

    amxc_string_new(&string_uri, 1);
    amxc_string_new(&partial_string, 1);
    amxc_string_new(&extracted_string, 1);
    amxc_string_set(string_uri, uri);

    rv = extract_regex_from_string(string_uri, "\\.[0-9]+$", partial_string);
    when_false((rv == 0), exit);
    rv = extract_regex_from_string(partial_string, "[0-9]+", extracted_string);
    when_false((rv == 0), exit);

    instance_index = atoi(extracted_string->buffer);

exit:
    amxc_string_delete(&string_uri);
    amxc_string_delete(&extracted_string);
    amxc_string_delete(&partial_string);
    return instance_index;
}

/**
   @brief
   Write string to a file.

   @param[in] fname file name.
   @param[in] string string containing the information to be written.
 */
void write_to_file(const char* fname, amxc_string_t* string) {
    FILE* p_file;

    if(access(fname, F_OK) == 0) {
        remove(fname);
    }

    p_file = fopen(fname, "w");
    fputs(string->buffer, p_file);
    fclose(p_file);
}

/**
   @brief
   Copy file.

   @param[in] fsrc Source file.
   @param[in] fdst Destination file.
 */
int copy_file(const char* fsrc, const char* fdst) {
    int c;
    FILE* stream_R;
    FILE* stream_W;

    assert_non_null(fsrc);
    assert_non_null(fdst);
    assert_string_not_equal(fsrc, "");
    assert_string_not_equal(fdst, "");
    assert_int_equal(access(fsrc, F_OK), 0);

    stream_R = fopen(fsrc, "r");
    if(stream_R == NULL) {
        return -1;
    }
    stream_W = fopen(fdst, "w");//create and write to file
    if(stream_W == NULL) {
        fclose(stream_R);
        return -2;
    }
    while((c = fgetc(stream_R)) != EOF) {
        fputc(c, stream_W);
    }
    fclose(stream_R);
    fclose(stream_W);

    assert_int_equal(access(fdst, F_OK), 0);
    return 0;
}

/**
 * event loop utils
 */
void event_loop_clean(void) {
    printf("%s\n", __FUNCTION__);
    if(ev_fd) {
        event_del(ev_fd);
        event_free(ev_fd);
        ev_fd = NULL;
    }
    if(base) {
        event_base_free(base);
        base = NULL;
    }
}


int event_loop_init(void) {
    int retval = -1;
    struct event_base* _base = NULL;
    struct event_config* cfg = NULL;

    cfg = event_config_new();
    when_null(cfg, exit);

    /* require an event method that allows file descriptors as well as sockets */
    event_config_require_features(cfg, EV_FEATURE_FDS);
    _base = event_base_new_with_config(cfg);
    if(_base == NULL) {
        /* epoll doesn't support regular Unix files, force libevent not to use epoll */
        event_config_require_features(cfg, 0);
        event_config_avoid_method(cfg, "epoll");
        _base = event_base_new_with_config(cfg);
    }

    event_config_free(cfg);

    when_null(_base, exit);

    base = _base;

    retval = 0;

exit:
    if(retval != 0) {
        event_loop_clean();
    }
    printf("%s %s\n", __FUNCTION__, (retval != 0) ? "failure" : "success"); fflush(stdout);
    return retval;
}

void event_loop_start(void) {
    printf("%s\n", __FUNCTION__); fflush(stdout);
    event_base_dispatch(base);
    // in case ftx_request_send config pre-check fails this is needed
    amxp_timers_calculate();
    amxp_timers_check();
}

/**
 * event loop fd ev_fd handler to notify libtransfer on fds activity @ref ftx_fd_event_handler
 */
static void event_loop_fd_event_cb(evutil_socket_t fd, UNUSED short what, UNUSED void* ptr) {
    printf("%s active fd:%d, notify lib filetransfer using ftx_fd_event_handler()\n", __FUNCTION__, fd); fflush(stdout);
    ftx_fd_event_handler((int) fd);
    event_base_loopbreak((struct event_base*) ptr);
}

/**
 * request fd set handler to add or remove fds from event loop, @ref ftx_init
 */
int fd_set_cb(int fd, bool add) {
    int retval = -1;

    if(add) {
        ev_fd = event_new(base, fd, EV_READ | EV_PERSIST, event_loop_fd_event_cb, base);
        when_null(ev_fd, exit);
        when_failed(event_add(ev_fd, NULL), exit);
    } else {
        when_failed(event_del(ev_fd), exit);
        event_free(ev_fd);
        ev_fd = NULL;
    }

    retval = 0;

exit:
    printf("%s %s event loop fd:%d %s\n", __FUNCTION__, add ? "adding to" : "removing from", (int) fd,
           (retval == 0) ? "success" : "failure");
    return retval;
}
