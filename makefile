include makefile.inc

NOW = $(shell date +"%Y-%m-%d(%H:%M:%S %z)")

# Extra destination directories
PKGDIR = ./output/$(MACHINE)/pkg/

define create_changelog
	@$(ECHO) "Update changelog"
	mv CHANGELOG.md CHANGELOG.md.bak
	head -n 9 CHANGELOG.md.bak > CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(ECHO) "## Release $(VERSION) - $(NOW)" >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(GIT) log --pretty=format:"- %s" $$($(GIT) describe --tags | grep -v "merge" | cut -d'-' -f1)..HEAD  >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	tail -n +10 CHANGELOG.md.bak >> CHANGELOG.md
	rm CHANGELOG.md.bak
endef

# targets
all:
	$(MAKE) -C src all

clean:
	$(MAKE) -C src clean

install: all
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/object/$(COMPONENT).so $(DEST)/usr/lib/amx/modules/$(COMPONENT).so
	$(INSTALL) -D -p -m 0755 scripts/get_rootfs_version.sh $(DEST)/usr/lib/amx/scripts/get_rootfs_version.sh
	$(INSTALL) -D -p -m 0755 scripts/get_rootfs_partlabel.sh $(DEST)/usr/lib/amx/scripts/get_rootfs_partlabel.sh
	$(INSTALL) -D -p -m 0755 scripts/kill_fit_tools.sh $(DEST)/usr/lib/amx/scripts/kill_fit_tools.sh

package: all
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/object/$(COMPONENT).so $(PKGDIR)/usr/lib/amx/modules/$(COMPONENT).so
	$(INSTALL) -D -p -m 0755 scripts/get_rootfs_version.sh $(PKGDIR)/usr/lib/amx/scripts/get_rootfs_version.sh
	$(INSTALL) -D -p -m 0755 scripts/get_rootfs_partlabel.sh $(PKGDIR)/usr/lib/amx/scripts/get_rootfs_partlabel.sh
	$(INSTALL) -D -p -m 0755 scripts/kill_fit_tools.sh $(PKGDIR)/usr/lib/amx/scripts/kill_fit_tools.sh
	cd $(PKGDIR) && $(TAR) -czvf ../$(COMPONENT)-$(VERSION).tar.gz .
	cp $(PKGDIR)../$(COMPONENT)-$(VERSION).tar.gz .
	make -C packages

changelog:
	$(call create_changelog)

test:
	$(MAKE) -C test run
	$(MAKE) -C test coverage

.PHONY: all clean changelog install package test