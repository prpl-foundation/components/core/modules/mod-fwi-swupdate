/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__MOD_COMMON_H__)
#define __MOD_COMMON_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>

#include <amxc/amxc_macros.h>
#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_action.h>
#include <amxb/amxb.h>
#include <amxo/amxo.h>
#include <amxm/amxm.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#define STRING_EMPTY(TEXT) ((TEXT == NULL) || (*TEXT == 0))
#define FREE(X) do { \
        if(X != NULL) { \
            free(X); \
            X = NULL; \
        } \
} while(0)
#define SAH_TRACEZ_WARNING_WITHOUT_SOURCE(zone, format, ...) sahTrace(TRACE_LEVEL_WARNING, "%s%-7.7s%s - %s[%s]%s" format "%s", SAHTRACE_ZONE(zone), SAHTRACE_WARNING, ## __VA_ARGS__)

// module names
#define MOD_DM_MNGR  "dm-mngr"
#define MOD_FWI_CTRL "firmware-ctrl"

#define DEFAULT_MODELNAME "myboard"
#define DEFAULT_HWREVISION "1.0"
#define DEFAULT_SW_VERSIONS "nominal master"

#define DEFAULT_PATH_SW_VERSIONS "/tmp/sw-versions"
#define DEFAULT_PATH_HW_REVISION "/tmp/hwrevision"
#define DEFAULT_PATH_SSL_CERTIFICATES "/etc/ssl/certs/"

#define STATUS_NOIMAGE     "NoImage"
#define STATUS_ACTIVE      "Active"
#define STATUS_DOWNLOADING "Downloading"
#define STATUS_VALIDATING  "Validating"
#define STATUS_AVAILABLE   "Available"
#define STATUS_DOWNFAILED  "DownloadFailed"
#define STATUS_VALIDFAILED "ValidationFailed"
#define STATUS_INSTFAILED  "InstallationFailed"
#define STATUS_ACTIVFAILED "ActivationFailed"

#define SCRIPT_VERSION_ROOTFS "/usr/lib/amx/scripts/get_rootfs_version.sh"
#define SCRIPT_PARTLABEL "/usr/lib/amx/scripts/get_rootfs_partlabel.sh"
#define SCRIPT_KILL_FIT_TOOLS "/usr/lib/amx/scripts/kill_fit_tools.sh"
#define STRING_SCRIPT_RESULT "+++Script Result: "
#define STRING_END_OF_SCRIPT "+++Finished script+++"

#define FLASH_SWU_FILE "flash.swu"
#define TMPFS_SCRIPT_USER "swupdate"
#define TMPFS_SCRIPT_MOUNT_PATH "/usr/lib/amx/scripts/tmpfs_download_mount.sh"
#define TMPFS_SCRIPT_UNMOUNT_PATH "/usr/lib/amx/scripts/tmpfs_download_unmount.sh"

#define TMPFS_SCRIPT_LOCK_FILE "/tmp/tmpfs_download_script.lock"

#define TMPFS_DOWNLOAD_DURATION_PARAM "DownloadPathDuration"
#define TMPFS_DEFAULT_DOWNLOAD_SIZE 100 //100 Mb

int set_parser(const char* function_name,
               amxc_var_t* args,
               amxc_var_t* ret);

amxo_parser_t* get_deviceinfo_parser(void);

int set_dm(const char* function_name,
           amxc_var_t* args,
           amxc_var_t* ret);
amxd_dm_t* get_dm(void);

int set_firmware_image(int index, const cstring_t key, const cstring_t value);
void set_fwi_path(const char* path);
void clean_fwi_path(void);
void set_fwi_status(const char* status, const char* faultString);
int get_fwi_index(void);
void init_filetransfer_ctx(int (* cb_function)(int, bool));
void clean_filetransfer_ctx(void);
const char* get_config(const char* config_param);
int32_t get_config_int32(const char* config_param);
uint32_t get_uint32_param(const char* parameter, bool with_prefix);
char* get_swu_path(void);
int is_path_directory(const char* path);
void delete_file(const char* file);
void set_busy_status(bool parameter_busy);
bool get_busy_status(void);
void read_from_file(const char* fname, amxc_string_t* string);
void read_from_fd(const int fd, amxc_string_t* string);
void split_string_lines(amxc_string_t* string, amxc_llist_t* lines);
void reboot_board(void);
int extract_regex_from_string(amxc_string_t* str, const cstring_t regex, amxc_string_t* extracted_str);
cstring_t execute_script_get_output(const cstring_t script, amxc_string_t* string_script_param);
int execute_tmpfs_script_get_output(const cstring_t script, int timeout_ms, amxc_string_t* output,
                                    const cstring_t p1, const cstring_t p2, const cstring_t p3, const cstring_t p4, const cstring_t p5);
int mount_tmpfs_partition(const char* user, unsigned int file_size_mb);
int unmount_tmpfs_partition(const char* user);

#ifdef __cplusplus
}
#endif

#endif // __MOD_COMMON_H__
