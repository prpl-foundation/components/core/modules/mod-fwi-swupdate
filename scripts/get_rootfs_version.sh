#!/bin/ash
export ROOTFS_INACTIVE="${1:-0}"

PRPL_VERSIONS_FILE="/tmp/sw-versions-prpl"

. /lib/prpl-layout-partlabels #Environment variables with the partitioning standard installed by the board-configurator package

fit_tools_version_cmd() {
    local dev_file="$1"
    fit_tools info "$dev_file" -q -p s,/version -u /security/sbl/rootfs.pubkey.raw 2> /dev/null
}

get_version_from_tmp() {
    local partlabel="$1"
    local file_content=""
    
    if [ -n "$partlabel" ]; then
        file_content=`cat $PRPL_VERSIONS_FILE | grep "$partlabel " 2> /dev/null || true`
        version=`echo "$file_content" | cut -d' ' -f2 2> /dev/null || true`

        if [ -z "$version" ]; then
            [ -f "/usr/bin/scan_prpl_partitions_versions" ] && /usr/bin/scan_prpl_partitions_versions &> /dev/null || true
            file_content=`cat $PRPL_VERSIONS_FILE | grep "$partlabel " 2> /dev/null || true`
            version=`echo "$file_content" | cut -d' ' -f2 2> /dev/null || true`
        fi

        echo "$version"
    fi
}

print_fit_version() {
    local dev_path="$1"
    local partlabel="$2"
    local fit_version=""
    for dev_file in "$dev_path"*; do
        if [ ! -f "$dev_file" ]; then
            if [ ! -L "$dev_file" ]; then
                continue
            fi
        fi
        fit_version=`fit_tools_version_cmd "$dev_file" 2> /dev/null || true`
        if [ -n "$fit_version" ]; then
            break
        fi
    done

    if [ -z "$fit_version" ]; then
        fit_version=`get_version_from_tmp "$partlabel" 2> /dev/null || true`
    fi

    echo "$fit_version"
}

set +e

if [ "$ROOTFS_INACTIVE" -eq 0 ]; then
    ROOTFS_DEV_PATH="$PRPL_LAYOUT_PART_PATH/$PRPL_LAYOUT_ROOTFS_ACTIVE" #All PRPL_LAYOUT_ variables come from the board-configurator's script /lib/prpl-layout-partlabels
else
    ROOTFS_DEV_PATH="$PRPL_LAYOUT_PART_PATH/$PRPL_LAYOUT_ROOTFS_INACTIVE" #All PRPL_LAYOUT_ variables come from the board-configurator's script /lib/prpl-layout-partlabels
fi

fit_version="`print_fit_version "$ROOTFS_DEV_PATH" "$PRPL_LAYOUT_ROOTFS_ACTIVE" 2> /dev/null`"

if [ ! -f "/usr/bin/convert_version_format" ]; then
    echo "$fit_version"
else
    # convert_version_format version path_to_rootfs
    # script taking the version number as the first argument and the rootfs path as a second argument
    # that apply formatting to the version number before it must return a string containing the formatted version number
    # the argument are provided as a base for generating the formatted version number
    /usr/bin/convert_version_format "$fit_version" "$ROOTFS_DEV_PATH"
fi

echo "+++Finished script+++"
sleep 5
