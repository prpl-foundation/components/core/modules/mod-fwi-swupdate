#!/bin/ash
export ROOTFS_INACTIVE="${1:-0}"

. /lib/prpl-layout-partlabels #Environment variables with the partitioning standard installed by the board-configurator package

set +e

if [ "$ROOTFS_INACTIVE" -eq 0 ]; then
    echo "$PRPL_LAYOUT_ROOTFS_ACTIVE" #All PRPL_LAYOUT_ variables come from the board-configurator's script /lib/prpl-layout-partlabels
else
    echo "$PRPL_LAYOUT_ROOTFS_INACTIVE" #All PRPL_LAYOUT_ variables come from the board-configurator's script /lib/prpl-layout-partlabels
fi

echo "+++Finished script+++"
sleep 5
