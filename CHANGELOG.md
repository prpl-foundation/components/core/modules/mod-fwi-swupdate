# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.5.0 - 2024-12-10(11:53:36 +0000)

### Fixes

- Reboot: Update module for compatibility with reboot service that implement TR-181 2.18

### Other

- [prpl][reboot-service] Implement TR-181 2.18 refactor Device.DeviceInfo.Reboots.{i}.Reason / add Device.DeviceInfo.Reboots.{i}.FirmwareUpdated (break compatibility)

## Release v0.4.10 - 2024-12-09(09:55:30 +0000)

### Other

- [Security]Script with 777 right in mod-fwi-swupdate

## Release v0.4.9 - 2024-09-26(14:29:02 +0000)

### Other

- - [tr181-deviceinfo] Unable to upgrade/downgrade to any firmware from via WebGUI

## Release v0.4.8 - 2024-09-24(10:35:36 +0000)

### Other

- [swupdate] Allow custom formatting of project version number

## Release v0.4.7 - 2024-09-11(16:04:32 +0000)

### Other

- - DeviceInfo.X_VZ-COM_LastSoftwareUpdateStamp and DeviceInfo.X_PRPL-COM_LastUpgradeDate has incorrect date

## Release v0.4.6 - 2024-09-05(11:20:04 +0000)

### Other

- - [swupdate] More flexible version number format

## Release v0.4.5 - 2024-09-02(15:53:07 +0000)

### Other

- - Firmware upgrade fails when /tmp is full

## Release v0.4.4 - 2024-07-18(10:34:26 +0000)

### Other

- - [swudate] use ash instead of bash in scritps

## Release v0.4.3 - 2024-06-28(10:16:01 +0000)

### Other

- - [mod-fwi-swupdate] Only UBI is supported  to get rootfs versions

## Release v0.4.2 - 2024-03-15(10:39:49 +0000)

### Fixes

- [FirmwareUpdate] Set correct reboot reason in case of reboot due to FirmwareUpdate

## Release v0.4.1 - 2023-11-29(09:39:38 +0000)

### Other

- Opensource component on prplfoundation gitlab

## Release v0.4.0 - 2023-11-22(10:25:48 +0000)

### Fixes

- All amxm destructors should let top application unloading them

## Release v0.3.4 - 2023-11-15(16:15:21 +0000)

### Fixes

- Use correct amxm module name to call update-status

## Release v0.3.3 - 2023-11-07(14:18:46 +0000)

### Fixes

- [mod-fwi-swupdate] WebUI upgrade failing:  Rescue scenario wrongly selected instead of nominal scenario

## Release v0.3.2 - 2023-10-19(14:39:53 +0000)

### Changes

- Remove default public singning key

## Release v0.3.1 - 2023-10-17(14:46:40 +0000)

### Fixes

- [swupdate][tr181-deviceinfo] Upgrade issues on Safran

## Release v0.3.0 - 2023-10-10(11:35:26 +0000)

### New

- [swupdate][mod_fwi-swupdate][libfiletransfer] Mutual authenticity and HTTPS support for Download() function

## Release v0.2.3 - 2023-09-15(14:32:50 +0000)

### Fixes

- DeviceInfo is blocking for a long time on boot

## Release v0.2.2 - 2023-08-31(09:32:50 +0000)

### Other

- Set SAH license

## Release v0.2.1 - 2023-08-31(09:26:17 +0000)

### Other

- Share component on gitlab.softathome.com

## Release v0.2.0 - 2023-08-17(09:11:54 +0000)

### New

- [DeviceInfo] Version information should be extracted from FIT header when secure bootloader is used

## Release v0.1.6 - 2023-08-03(11:41:38 +0000)

### Other

- [DeviceInfo][mod_fwi-swupdate] sw-version file is wrongly formated

## Release v0.1.5 - 2023-08-02(13:01:28 +0000)

### Other

- [SWUpdate][mod_fwi-swupdate] module should reboot the board on successful activation

## Release v0.1.4 - 2023-07-26(14:52:06 +0000)

### Other

- [SWUpdate][mod_fwi-swupdate] Create hw-revision and sw-version file on module startup

## Release v0.1.3 - 2023-05-30(09:47:09 +0000)

### Other

- [DeviceInfo][mod_fwi-swupdate] fix hwrevision to "<ModelName> 1.0" and change swupdate command

## Release v0.1.2 - 2023-04-04(11:51:20 +0000)

### Other

- [SWUpdate][DeviceInfo] Automatically delete swu file if the update fails.

## Release v0.1.1 - 2023-03-02(08:55:53 +0000)

### Other

- [prpl][SWUpdate][DeviceInfo] Use storage swu file on Download function

## Release v0.1.0 - 2022-11-17(12:10:20 +0000)

### Other

- - [libfiletransfer] add reply handler to request new API arguments

## Release v0.0.2 - 2022-10-11(07:54:54 +0000)

### Other

- FirmwareImage activate fails when update file not present

## Release v0.0.1 - 2022-09-27(15:42:36 +0000)

### Fixes

- Remove pedantic flag

### Other

- Implement swupdate on wnc board

